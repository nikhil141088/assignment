<div class="container">
<div class="page-header">
  <h1>List of Hotels <small>Book Now</small></h1>
</div>  
</div>
<div class="container">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<form method="GET" action="<?php echo site_url(); ?>hotels/lists">
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
  <div class="form-group">
    <label for="">Name</label>
    <input type="text" class="form-control" id="" value="<?php echo $this->input->get('name',TRUE); ?>"  name="name"  placeholder="Enter Hotel Name" autocomplete="off" autofocus>
  </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
  <div class="form-group">
    <label for="">City</label>
    <input type="text" class="form-control" id="" name="city" value="<?php echo $this->input->get('city',TRUE); ?>" placeholder="Enter City" autocomplete="off" autofocus>
  </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
  <div class="form-group">
    <label for="">Price($)</label>
    <input type="text" class="form-control" id="" name="price" value="<?php echo $this->input->get('price',TRUE); ?>" placeholder="Enter Price" autocomplete="off" autofocus>
  </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
  <div class="form-group">
  <label for=""></label>
    <input type="submit" class="form-control btn btn-success" id="search-btn" value="SEARCH">
  </div>
  </div>
</form>
</div>
</div>
<?php if($this->session->userdata('ni_user_role') == 2){ ?>
<div class="container">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<form id="sort-form" method="GET" action="<?php echo site_url(); ?>hotels/lists">
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
  <div class="form-group">
    <label for="">Sort</label>
    <select class="form-control" name="sort">
    	<option value="">Sort Option</option>
    	<option value="name" <?php if($this->input->get('sort',TRUE) == "name") echo "selected"; ?>>Name</option>
    	<option value="price" <?php if($this->input->get('sort',TRUE) == "price") echo "selected"; ?>>Price</option>
    </select>
  </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
  <div class="form-group">
  <label for=""></label>
    <input type="submit" class="form-control btn btn-info" id="search-btn" value="SORT">
  </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
  <div class="form-group">
  <label for=""></label>
  <a href="<?php echo site_url(); ?>hotels/lists"><input type="button" class="form-control btn btn-danger" id="search-btn" value="CLEAR"></a>
  </div>
  </div>
  </form>
</div>
</div>
<?php } ?>
<div class="container">
<?php 
if(count($list->hotels) > 0){
$i = 1; foreach ($list->hotels as $key => $value) { ?>
<div class="panel panel-success">
  <div class="panel-heading">Hotel : <?php echo ucfirst($value->name); ?> | City :<?php echo $value->city; ?> | Price :  <?php echo "$".$value->price; ?></div>
  <div class="panel-body">
    Availability : 
    <?php foreach ($value->availability as $key => $val) { ?>
     <span>From : <?php echo $val->from ?> - To : <?php echo $val->to ?> | </span>   
    <?php } ?>
  </div>
</div>
<?php } } else {?>
<div class="panel panel-danger">
  <div class="panel-heading">Hotel Lists</div>
  <div class="panel-body">No Results Found</div>
</div>
<?php } ?>
</div><!-- /container -->