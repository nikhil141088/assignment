<div class="container">
        <div class="card card-container">
            <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
            <p id="profile-name" class="profile-name-card"></p>
           <!--  <form class="form-signin" action=""> -->
           <?php echo form_open(site_url().'authenticate/login/', 'class="form-signin" id="authenticate_form" method="POST" onsubmit="return validate();" '); ?>
                <?php if ($this->session->flashdata('Error') != "") { ?>
                <!-- ALERT -->
                <div class="alert alert-mini alert-danger margin-bottom-30">
                <strong>Ohh! </strong> <?php echo $this->session->flashdata('Error'); ?>
                </div><!-- /ALERT -->
                <?php } ?>
                <div id="msg"></div>
                <input type="hidden" name="productkey" value="<?php echo USERTESTPRODUCT; ?>">
                <input type="hidden" name="method" value="login">
                <input type="hidden" name="endpoint" value="<?php echo ENDPOINT; ?>login/">
                <input type="hidden" name="type" value="POST">

                <input type="email" name="email" autocomplete="off" id="inputEmail" class="form-control" placeholder="Email Address"  required autofocus>
                <input type="password" name="password" autocomplete="off" id="inputPassword" class="form-control" placeholder="Password" required>
                <button id="login-btn" class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign In</button>
             <?php echo form_close(); ?>
        </div><!-- /card-container -->
    </div><!-- /container -->