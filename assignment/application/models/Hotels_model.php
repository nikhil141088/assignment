<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotels_model extends CI_Model {

        /*Author: Nikhil Gowda
          @Parms : URL,PARAMETERS,TYPE
          Date : 28-03-2018
          Instance : Call CURL
        */
        public function call($_url, $_param='',$_type){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //curl_setopt($ch, CURLOPT_HEADER, false); 
        if($_type == "POST"){
                        $postData = '';
                        foreach($_param as $k => $v) 
                        { 
                        $postData .= $k . '='.$v.'&'; 
                        }
                        rtrim($postData, '&');  
                        curl_setopt($ch, CURLOPT_POST, count($postData));
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        }    
        $output=curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    /*Author: Nikhil Gowda
      @Parms : URL,PARAMETERS,TYPE
      Date : 26-03-2018
      Instance : Get Hotels List via Curl
    */
    public function get_hotels(){
       //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        $user_id = $this->session->userdata('ni_user_id');
        $user_productkey = USERTESTPRODUCT;
        $secretkey = $this->session->userdata('ni_secret_key');
        $role_type = $this->session->userdata('ni_user_role');
        $method = "hotels_list";
        $endpoint = site_url()."wsv1/hotels_list/";
        $type = "POST";
        $param = array(
        "user_id" => $user_id,
        "productkey" => $user_productkey,
        "secretkey" =>$secretkey,
        "role_type"=>$role_type,
        "method" =>$method,
        "endpoint" =>$endpoint,
        "type" =>$type,
        );
        $url = $endpoint;
        $result = $this->call($url, $param, $type);
        $response = json_decode($result);
       // $this->cache->save('hotels', $result, 300);
        return $response;
        }

        /*Author: Nikhil Gowda
        @Parms : URL,PARAMETERS,TYPE
        Date : 26-03-2018
        Instance : Get Hotels Serach List via Curl
        */
        public function get_search_hotels($name,$city,$price){
            //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
            $user_id = $this->session->userdata('ni_user_id');
            $user_productkey = USERTESTPRODUCT;
            $secretkey = $this->session->userdata('ni_secret_key');
            $role_type = $this->session->userdata('ni_user_role');
            $method = "hotel_search";
            $endpoint = site_url()."wsv1/hotel_search/";
            $type = "POST";
            $param = array(
            "user_id" => $user_id,
            "productkey" => $user_productkey,
            "secretkey" =>$secretkey,
            "role_type"=>$role_type,
            "method" =>$method,
            "endpoint" =>$endpoint,
            "type" =>$type,
            "name" =>$name,
            "city" =>$city,
            "price" =>$price
            );
            $url = $endpoint;
            $result = $this->call($url, $param, $type);
           // $this->cache->save('hotels', $result, 300);
            return $response = json_decode($result);
        }

        /*Author: Nikhil Gowda
        @Parms : URL,PARAMETERS,TYPE
        Date : 26-03-2018
        Instance : Get Hotels Sort List via Curl
        */
        public function get_sort_hotels($sort){
            //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
            $user_id = $this->session->userdata('ni_user_id');
            $user_productkey = USERTESTPRODUCT;
            $secretkey = $this->session->userdata('ni_secret_key');
            $role_type = $this->session->userdata('ni_user_role');
            $method = "hotel_sort";
            $endpoint = site_url()."wsv1/hotel_sort/";
            $type = "POST";
            $param = array(
            "user_id" => $user_id,
            "productkey" => $user_productkey,
            "secretkey" =>$secretkey,
            "role_type"=>$role_type,
            "method" =>$method,
            "endpoint" =>$endpoint,
            "type" =>$type,
            "sort" =>$sort,
            );
            $url = $endpoint;
            $result = $this->call($url, $param, $type);
           // $this->cache->save('hotels', $result, 300);
            return $response = json_decode($result);
        }
	 
}
