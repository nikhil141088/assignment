<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authenticate_model extends CI_Model {

        /*Author: Nikhil Gowda
        @Parms : id,email,role_type,date,secret_key
        Date : 27-03-2018
        Instance : Setting login sessions when user logins
        */
	public function login_session($id,$email,$role_type,$date,$secret_key){
		$session_data = array(
                'ni_user_id'        =>  $id,
                'ni_user_email'      =>  $email,
                'ni_user_role'  =>  $role_type,
                'ni_user_date'  =>  $date,
                'ni_secret_key'  =>  $secret_key,
                );
        $this->session->set_userdata($session_data);
        return TRUE;
	}
	 
}
