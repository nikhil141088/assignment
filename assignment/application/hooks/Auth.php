<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth
{

    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();

        if(!isset($this->CI->session)){  //Check if session lib is loaded or not
              $this->CI->load->library('session');  //If not loaded, then load it here
        }
    }

   function loginCheck()
   {
            if($this->CI->session->userdata('ni_user_id') == '')
            {
                
                if($this->CI->router->class != 'Authenticate' && $this->CI->router->class != 'authenticate'){
                    redirect(base_url()); 
                }
            }
    }
}
 
