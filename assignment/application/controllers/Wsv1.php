<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wsv1 extends CI_Controller {
	 function __construct() {
	    parent::__construct();
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	/*Author: Nikhil Gowda
	@Parms : url
	Date : 26-03-2018
	Instance : Get Curl of list of Hotels
	*/
	public function call_hotels($_url)
        {
        	// $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
                $handle=curl_init($_url);
                curl_setopt($handle, CURLOPT_VERBOSE, true);
                curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
                $content = curl_exec($handle);
                //$this->cache->save('hotels', $content, 300);
                return $content; // show target page
        }

	/*Author: Nikhil Gowda
	@Parms : JSON string
	Date : 26-03-2018
	Instance : Check JSON string or not
	*/
    public function isJson($string) 
    {
            return ((is_string($string) &&
            (is_object(json_decode($string)) ||
            is_array(json_decode($string))))) ? true : false;
    } 

	/*Author: Nikhil Gowda
	  @Parms : method,type,endpoint
	  Date : 26-03-2018
	  Instance : Checking for API Method
	*/
	public function check_method($method,$type,$endpoint){
		switch ($method) {
			case 'login':
				if($type != "POST" || $endpoint != ENDPOINT."login/")
					return 420;
				break;
			case 'hotels_list':
				if($type != "POST" || $endpoint != ENDPOINT."hotels_list/")
					return 420;
				break;
			case 'hotel_search':
				if($type != "POST" || $endpoint != ENDPOINT."hotel_search/")
					return 420;
				break;
			case 'hotel_sort':
				if($type != "POST" || $endpoint != ENDPOINT."hotel_sort/")
					return 420;
				break;
			default:
				return 410;
				break;
		}
	}

	/*Author: Nikhil Gowda
	  @Parms : user productkey,admin productkey
	  Date : 26-03-2018
	  Instance : Checking for User Prodyct Key
	*/
	public function check_productkey($user_productkey,$admin_productkey){
		if($user_productkey != $admin_productkey){
			return 500;
		}
	}

	/*Author: Nikhil Gowda
	  @Parms : id,secretkey
	  Date : 26-03-2018
	  Instance : Checking for User Secret Key
	*/
	public function check_secretkey($id,$secretkey){
		$users = unserialize(USERS);
		foreach ($users as $key => $value) {
			if($key == $id)
			{
				if($secretkey != $value['authorization']){
					return 501;
				}
			
			}
		}
	}

	/*Author: Nikhil Gowda
	  @Parms : id,role type
	  Date : 26-03-2018
	  Instance : Checking for User Role Type - 1-> Normal User , 2-> Privilege User
	*/
	public function check_roletype($id,$role_type){
		$users = unserialize(USERS);
		foreach ($users as $key => $value) {
			if($key == $id)
			{
				if($role_type != $value['type']){
					return 504;
				}
			
			}
		}
	}


	/*Author: Nikhil Gowda
	  @Parms : Email,Password
	  Date : 26-03-2018
	  Instance : User Logon API
	*/
	public function login(){
		header('Access-Control-Allow-Origin: *');  //Setting headers for cross platform
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT');
		header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
		header('Content-Type: application/json');
		$response = array();
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[16]');
		$this->form_validation->set_rules('productkey', 'Product Key', 'trim|required');
		$this->form_validation->set_rules('method', 'Method', 'trim|required');
		$this->form_validation->set_rules('endpoint', 'Endpoint URL', 'trim|required');
		$this->form_validation->set_rules('type', 'type', 'trim|required');
	     if ($this->form_validation->run() == TRUE)
		 {
			$email = $this->input->post('email',TRUE);
			$email = strtolower($email);
			$password = md5($this->input->post('password',TRUE));
			$user_productkey = $this->input->post('productkey',TRUE);
			$method = $this->input->post('method',TRUE);
			$endpoint = $this->input->post('endpoint',TRUE);
			$type = $this->input->post('type',TRUE);
			$admin_productkey = ADMINTESTPRODUCTKEY;

			// Check for Access Key
			if($this->check_productkey($user_productkey,$admin_productkey) == 500){
				$response['status'] = "500";
				$response['message'] = "Access Denied";
			}else{

				// Check for Type and Method
				if($this->check_method($method,$type,$endpoint) == 420){
					$response['status'] = "420";
					$response['message'] = "Invalid Type or Endpoint";

					// Check for Method
				}else if($this->check_method($method,$type,$endpoint) == 410){
					$response['status'] = "410";
					$response['message'] = "Invalid Method";
				}else{

					// Users Search logic
					$users = unserialize(USERS);
					$auth = "";
					$id = "";
					$role_type= "";
					foreach ($users as $key => $value) {
						if($value['email'] == $email && $value['password'] == $password)
						{
							$auth = $value['authorization'];
							$id = $key;
							$role_type = $value['type'];
						}
					}
					if($auth != ""){
						$response['status'] = "200";
						$response['message'] = "Login Successfully";
						$response['id'] = $id;
						$response['email'] =$email;
						$response['role_type'] =$role_type;
						$response['secret_key'] =$auth;
						$response['date'] =date('Y-m-d H:i:s');
					}else if($auth == ""){
						$response['status'] = "100";
						$response['message'] = "Invalid User";
					}else{
						$response['status'] = "300";
						$response['message'] = "Invalid Request";
					}
				}
				}
			}else{
				$response['status'] = "900";
				$response['message'] = "Validation Error : ".strip_tags(trim(validation_errors()));
				}
		echo json_encode($response,JSON_NUMERIC_CHECK);
    }

    /*Author: Nikhil Gowda
	  @Parms : user_id,user_productkey,user_secretkey,role_type,method,endpoint,type,admin_productkey
	  Date : 26-03-2018
	  Instance : Get Hotels List API
	*/
	public function hotels_list(){

		header('Access-Control-Allow-Origin: *');  //Setting headers for cross platform
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT');
		header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
		header('Content-Type: application/json');

		//$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		$response = array();
		$this->form_validation->set_rules('user_id', 'User id', 'trim|required');
		$this->form_validation->set_rules('productkey', 'Product Key', 'trim|required');
		$this->form_validation->set_rules('secretkey', 'Secret Key', 'trim|required');
		$this->form_validation->set_rules('role_type', 'Role Type', 'trim|required');
		$this->form_validation->set_rules('method', 'Method', 'trim|required');
		$this->form_validation->set_rules('endpoint', 'Endpoint URL', 'trim|required');
		$this->form_validation->set_rules('type', 'type', 'trim|required');
		if ($this->form_validation->run() == TRUE)
		 {
		 	$user_id = $this->input->post('user_id',TRUE);
		 	$user_productkey = $this->input->post('productkey',TRUE);
		 	$user_secretkey = $this->input->post('secretkey',TRUE);
		 	$role_type = $this->input->post('role_type',TRUE);
			$method = $this->input->post('method',TRUE);
			$endpoint = $this->input->post('endpoint',TRUE);
			$type = $this->input->post('type',TRUE);
			$admin_productkey = ADMINTESTPRODUCTKEY;
			// Check for Access Key
			if($this->check_productkey($user_productkey,$admin_productkey) == 500){
				$response['status'] = "500";
				$response['message'] = "Access Denied";
			}else{

				// Check for Type and Method
				if($this->check_method($method,$type,$endpoint) == 420){
					$response['status'] = "420";
					$response['message'] = "Invalid Type or Endpoint";

					// Check for Method
				}else if($this->check_method($method,$type,$endpoint) == 410){
					$response['status'] = "410";
					$response['message'] = "Invalid Method";
				}else{

					// Check for Authorization
					if($this->check_secretkey($user_id,$user_secretkey) == 501){
						$response['status'] = "501";
						$response['message'] = "Invalid Authorization";
					}else{

				$url = "https://api.myjson.com/bins/tl0bp";
				$list = $this->call_hotels($url);
				if($this->isJson($list) == TRUE){
					//$this->cache->save('list', $list, 300);
					$response['status'] = "201";
					$response['message'] = "Hotel List";
					$response['list'] = $list;
				}else if($this->isJson($list) == FALSE){
					$response['status'] = "403";
					$response['message'] = "Invalid Json";
				}else if($list == ""){
					$response['status'] = "403";
					$response['message'] = "Invalid Json";
				}
				}
			}
			}
		}else{
				$response['status'] = "900";
				$response['message'] = "Validation Error : ".strip_tags(trim(validation_errors()));
				}
      echo json_encode($response,JSON_NUMERIC_CHECK);         
	}


	 /*Author: Nikhil Gowda
	  @Parms : name,city,price,list
	  Date : 26-03-2018
	  Instance : Search Hotel Callback function 
	*/
	public function search($name='',$city='',$price='',$list=''){
		$list = json_decode($list);
		$name = str_replace("%20"," ",$name);
		$city = str_replace("%20"," ",$city);
		$price = str_replace("%20"," ",$price);
		$price = floor($price);
		$arrkey = "";
		$result_array['hotels'] = array();
		foreach ($list->hotels as $key => $value) {
			if($value->name == $name || floor($value->price) == $price || $value->city == $city){
				array_push($result_array['hotels'], $value);
			}
		}
		return $result_array;
	}

	/*Author: Nikhil Gowda
	  @Parms : name,city,price,list
	  Date : 26-03-2018
	  Instance : Sort Hotel Callback function 
	*/
	public function sort($col="",$list=""){
		$list = json_decode($list);
		$list = $list->hotels;
		$sort = array();
		foreach ($list as $i => $obj) {
		$sort[$i] = $obj->{$col};
		}
		$sorted = array_multisort($sort, SORT_ASC, $list);
		return $list;

	}

	/*Author: Nikhil Gowda
	  @Parms : user_id,user_productkey,user_secretkey,role_type,method,endpoint,type,name,city,price
	  Date : 26-03-2018
	  Instance : Get Hotels Search API
	*/
	public function hotel_search(){
		header('Access-Control-Allow-Origin: *');  //Setting headers for cross platform
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT');
		header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
		header('Content-Type: application/json');
		//$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		$response = array();
		$this->form_validation->set_rules('user_id', 'User id', 'trim|required');
		$this->form_validation->set_rules('productkey', 'Product Key', 'trim|required');
		$this->form_validation->set_rules('secretkey', 'Secret Key', 'trim|required');
		$this->form_validation->set_rules('role_type', 'Role Type', 'trim|required');
		$this->form_validation->set_rules('method', 'Method', 'trim|required');
		$this->form_validation->set_rules('endpoint', 'Endpoint URL', 'trim|required');
		$this->form_validation->set_rules('type', 'type', 'trim|required');
		$this->form_validation->set_rules('name', 'Name', 'trim');
		$this->form_validation->set_rules('city', 'City', 'trim');
		$this->form_validation->set_rules('price', 'Price', 'trim');
		if ($this->form_validation->run() == TRUE)
		 {
		 	$user_id = $this->input->post('user_id',TRUE);
		 	$user_productkey = $this->input->post('productkey',TRUE);
		 	$user_secretkey = $this->input->post('secretkey',TRUE);
		 	$role_type = $this->input->post('role_type',TRUE);
			$method = $this->input->post('method',TRUE);
			$endpoint = $this->input->post('endpoint',TRUE);
			$type = $this->input->post('type',TRUE);
			$name = $this->input->post('name',TRUE);
			$city = $this->input->post('city',TRUE);
			$price = $this->input->post('price',TRUE);
			$admin_productkey = ADMINTESTPRODUCTKEY;

			// Check for Access Key
			if($this->check_productkey($user_productkey,$admin_productkey) == 500){
				$response['status'] = "500";
				$response['message'] = "Access Denied";
			}else{

				// Check for Type and Method
				if($this->check_method($method,$type,$endpoint) == 420){
					$response['status'] = "420";
					$response['message'] = "Invalid Type or Endpoint";

					// Check for Method
				}else if($this->check_method($method,$type,$endpoint) == 410){
					$response['status'] = "410";
					$response['message'] = "Invalid Method";
				}else{

					// Check for Authorization
					if($this->check_secretkey($user_id,$user_secretkey) == 501){
						$response['status'] = "501";
						$response['message'] = "Invalid Authorization";
					}else{

				$url = "https://api.myjson.com/bins/tl0bp";
				$list = $this->call_hotels($url);
				if($this->isJson($list) == TRUE){
					//$this->cache->save('list', $list, 300);
					$list = $this->search($name,$city,$price,$list);
					
					if(sizeof($list['hotels']) == 0){
					$response['status'] = "207";
					$response['message'] = "No Hotels Found";
					$response['list'] = "";
					}else{
					$response['status'] = "202";
					$response['message'] = "Hotel Search List";
					$response['list'] = $list;
					}
				}else if($this->isJson($list) == FALSE){
					$response['status'] = "403";
					$response['message'] = "Invalid Json";
				}else if($list == ""){
					$response['status'] = "403";
					$response['message'] = "Invalid Json";
				}
					}
				}
			}
		}else{
				$response['status'] = "900";
				$response['message'] = "Validation Error : ".strip_tags(trim(validation_errors()));
				}
      echo json_encode($response,JSON_NUMERIC_CHECK);    
	}

	/*Author: Nikhil Gowda
	  @Parms : user_id,user_productkey,user_secretkey,role_type,method,endpoint,sort
	  Date : 26-03-2018
	  Instance : Get Hotels Sort API
	*/
	public function hotel_sort(){
		header('Access-Control-Allow-Origin: *');  //Setting headers for cross platform
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT');
		header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
		header('Content-Type: application/json');
		//$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		$response = array();
		$this->form_validation->set_rules('user_id', 'User id', 'trim|required');
		$this->form_validation->set_rules('productkey', 'Product Key', 'trim|required');
		$this->form_validation->set_rules('secretkey', 'Secret Key', 'trim|required');
		$this->form_validation->set_rules('role_type', 'Role Type', 'trim|required');
		$this->form_validation->set_rules('method', 'Method', 'trim|required');
		$this->form_validation->set_rules('endpoint', 'Endpoint URL', 'trim|required');
		$this->form_validation->set_rules('type', 'type', 'trim|required');
		$this->form_validation->set_rules('sort', 'Sort', 'trim');
		if ($this->form_validation->run() == TRUE)
		 {
		 	$user_id = $this->input->post('user_id',TRUE);
		 	$user_productkey = $this->input->post('productkey',TRUE);
		 	$user_secretkey = $this->input->post('secretkey',TRUE);
		 	$role_type = $this->input->post('role_type',TRUE);
			$method = $this->input->post('method',TRUE);
			$endpoint = $this->input->post('endpoint',TRUE);
			$type = $this->input->post('type',TRUE);
			$sort = $this->input->post('sort',TRUE);
			$admin_productkey = ADMINTESTPRODUCTKEY;
			if($sort == "name" || $sort == "price"){

			if($role_type == 2){ // checking for Role Type Previlege

			// Check for Access Key
			if($this->check_productkey($user_productkey,$admin_productkey) == 500){
				$response['status'] = "500";
				$response['message'] = "Access Denied";
			}else{

				// Check for Type and Method
				if($this->check_method($method,$type,$endpoint) == 420){
					$response['status'] = "420";
					$response['message'] = "Invalid Type or Endpoint";

					// Check for Method
				}else if($this->check_method($method,$type,$endpoint) == 410){
					$response['status'] = "410";
					$response['message'] = "Invalid Method";
				}else{

					// Check for Authorization
					if($this->check_secretkey($user_id,$user_secretkey) == 501){
						$response['status'] = "501";
						$response['message'] = "Invalid Authorization";
					}else{
						if($this->check_roletype($user_id,$role_type) == 504){
						$response['status'] = "502";
						$response['message'] = "Invalid Role";
					}else{
				$url = "https://api.myjson.com/bins/tl0bp";
				$list = $this->call_hotels($url);
				if($this->isJson($list) == TRUE){
					//$this->cache->save('list', $list, 300);
					$list = $this->sort($sort,$list);
					$response['status'] = "203";
					$response['message'] = "Hotel Sort by ".$sort." List";
					$response['list']['hotels'] = $list;
				}else if($this->isJson($list) == FALSE){
					$response['status'] = "403";
					$response['message'] = "Invalid Json";
				}else if($list == ""){
					$response['status'] = "403";
					$response['message'] = "Invalid Json";
				}
					}
				}
				}
			}
		}else{
		$response['status'] = "505";
		$response['message'] = "Permission Denied";
	}
	}else{
		$response['status'] = "506";
		$response['message'] = "Invalid Sort value";
	}
	}else{
				$response['status'] = "900";
				$response['message'] = "Validation Error : ".strip_tags(trim(validation_errors()));
				}
      echo json_encode($response,JSON_NUMERIC_CHECK);    
	}


	/*Author: Nikhil Gowda
	  @Parms : GET- code
	  Date : 26-03-2018
	  Instance : Get Status Code API
	*/
	public function status_code(){
		header('Access-Control-Allow-Origin: *');  //Setting headers for cross platform
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT');
		header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
		header('Content-Type: application/json');
		if((isset($_GET['code']) && $_GET['code']!="" ) ){
			$response = array();
			switch ($_GET['code']) {

				case '100':
					$response['status'] = 100;
					$response['message'] = "Invalid User";
					$response['description'] = "User Email or Password is invalid";
					break;

				case '200':
					$response['status'] = 200;
					$response['message'] = "Login Successfully";
					$response['description'] = "You have login successfully to the application";
					break;

				case '201':
					$response['status'] = 201;
					$response['message'] = "Hotel List";
					$response['description'] = "Get the list of hotels";
					break;

				case '202':
					$response['status'] = 202;
					$response['message'] = "Hotel Search List";
					$response['description'] = "Get the list of hotels based on search filters";
					break;

				case '203':
					$response['status'] = 203;
					$response['message'] = "Hotel Sort List";
					$response['description'] = "Get the list of hotels based on sort filters";
					break;

				case '207':
					$response['status'] = 207;
					$response['message'] = "No Hotels Found";
					$response['description'] = "No Hotels found from the Api";
					break;

				case '300':
					$response['status'] = 300;
					$response['message'] = "Invalid Request";
					$response['description'] = "Error on your request";
					break;

				case '403':
					$response['status'] = 403;
					$response['message'] = "Invalid Json";
					$response['description'] = "Response Json is invalid";
					break;

					case '420':
					$response['status'] = 420;
					$response['message'] = "Invalid Type or Endpoint";
					$response['description'] = "Http Request Method is Invalid and the Api endpoint URL is invalid";
					break;

					case '410':
					$response['status'] = 410;
					$response['message'] = "Invalid Method";
					$response['description'] = "API method is invalid";
					break;

				case '500':
					$response['status'] = 500;
					$response['message'] = "Access Denied";
					$response['description'] = "Product Key is Invalid, Please contact admin";
					break;

				case '501':
					$response['status'] = 501;
					$response['message'] = "Invalid Authorization";
					$response['description'] = "Your Authorization id invalid";
					break;

				case '502':
					$response['status'] = 502;
					$response['message'] = "Invalid Role";
					$response['description'] = "User Role type is Invalid";
					break;

				case '505':
					$response['status'] = 505;
					$response['message'] = "Permission Denied";
					$response['description'] = "Normal Users do not have permission for sort";
					break;

				case '506':
					$response['status'] = 506;
					$response['message'] = "Invalid Sort value";
					$response['description'] = "Sort value should be name or price";
					break;

				case '900':
					$response['status'] = 900;
					$response['message'] = "Validation Error";
					$response['description'] = "Validation Error of your request";
					break;
				
				default:
					$response['status'] = 700;
					$response['message'] = "Invalid GET request";
					$response['description'] = "Invalid GET request to the API";
					break;
			}
		}else{
					$response['status'] = 700;
					$response['message'] = "Invalid GET request";
					$response['description'] = "Invalid GET request to the API";
		}
		 echo json_encode($response,JSON_NUMERIC_CHECK);   
		}

	
}
