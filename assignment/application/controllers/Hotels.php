<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotels extends CI_Controller {

	 function __construct() {
	    parent::__construct();
	    if($this->session->userdata('ni_user_email') == "")
	    {
	    	 redirect(base_url()); 
	    }
	    $this->load->model('hotels_model', 'hot');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	/*Author: Nikhil Gowda
	  @Parms : NULL , GET-name,city,price
	  Date : 28-03-2018
	  Instance : Call CURL to get list of Hotels
	*/
	  public function lists(){	
	  	$data['title'] = HOTEL_TITLE;
		$data['description'] = HOTEL_DESCRIPTION;
		$data['page'] = "lists";
		if((isset($_GET['name']) && $_GET['name']!="" )|| (isset($_GET['city']) && $_GET['city']!="" ) || (isset($_GET['price']) && $_GET['price']!="") ){
			$name = $this->input->get('name',TRUE);
			$city = $this->input->get('city',TRUE);
			$price = $this->input->get('price',TRUE);

		 	$list = $this->hot->get_search_hotels($name,$city,$price);
			if($list->status == 202){
				$hotels_list = $list->list;
				$data['list'] = $hotels_list;
				$data['message'] = $list->message;
				$data['status'] = $list->status;
			}else{
				$data['message'] = $list->message;
				$data['status'] = $list->status;
				$data['list'] = "";
			}
		}else if(isset($_GET['sort']) && $_GET['sort'] != ""){
			$sort = $this->input->get('sort',TRUE);
			$list = $this->hot->get_sort_hotels($sort);

			if($list->status == 203){
				$hotels_list = $list->list;
				$data['list'] = $hotels_list;
				$data['message'] = $list->message;
				$data['status'] = $list->status;
			}else{
				$data['message'] = $list->message;
				$data['status'] = $list->status;
				$data['list'] = "";
			}
		}else{
			$list = $this->hot->get_hotels();
			if($list->status == 201){
				$hotels_list = json_decode($list->list);
				$data['list'] = $hotels_list;
				$data['message'] = $list->message;
				$data['status'] = $list->status;
			}else{
				$data['message'] = $list->message;
				$data['status'] = $list->status;
				$data['list'] = "";
			}
		}
		$this->output->cache(0.05);
		$this->load->view('template',$data);
	  }
}
