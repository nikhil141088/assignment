<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authenticate extends CI_Controller {

	 function __construct() {
	    parent::__construct();
	    $this->load->model('authenticate_model', 'auth');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	/*Author: Nikhil Gowda
	  @Parms : URL,PARAMETERS,TYPE
	  Date : 27-03-2018
	  Instance : Call CURL
	*/
	public function call($_url, $_param='',$_type){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, false); 
        if($_type == "POST"){
			$postData = '';
			foreach($_param as $k => $v) 
			{ 
			$postData .= $k . '='.$v.'&'; 
			}
			rtrim($postData, '&');	
			curl_setopt($ch, CURLOPT_POST, count($postData));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        }    
        $output=curl_exec($ch);
        curl_close($ch);
        return $output;
    }

	/*Author: Nikhil Gowda
	  @Parms : null
	  Date : 27-03-2018
	  Instance : Login View Page
	*/
	public function index()
	{
		$this->output->delete_cache('Hotels');
       	$this->output->delete_cache('Authenticate');
       	$this->output->delete_cache();
        $this->session->sess_destroy();
		$data['title'] = LOGIN_TITLE;
		$data['description'] = LOGIN_DESCRIPTION;
		$data['page'] = "login";
		//$this->output->cache(0.05);
		$this->load->view('template',$data);
	}

	/*Author: Nikhil Gowda
	  @Parms : null
	  Date : 27-03-2018
	  Instance : Authenticating Login 
	*/
	public function login(){
		
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]|max_length[16]');
		$this->form_validation->set_rules('productkey', 'Product Key', 'trim|required');
		$this->form_validation->set_rules('method', 'Method', 'trim|required');
		$this->form_validation->set_rules('endpoint', 'Endpoint URL', 'trim|required');
		$this->form_validation->set_rules('type', 'type', 'trim|required');

		if ($this->form_validation->run() == TRUE)
		{
			$email = $this->input->post('email',TRUE);
			$password = $this->input->post('password',TRUE);
			$productkey = $this->input->post('productkey',TRUE);
			$method = $this->input->post('method',TRUE);
			$endpoint = $this->input->post('endpoint',TRUE);
			$type = $this->input->post('type',TRUE);


			$param = array(
			"email" => $email,
			"password" => $password,
			"productkey" =>$productkey,
			"method" =>$method,
			"endpoint" =>$endpoint,
			"type" =>$type,
			);
			$url = ENDPOINT.'login/';
			
			$result = $this->call($url, $param, $type);
			$response = json_decode($result);
			
			if($response->status == 200){
				$log_sessions = $this->auth->login_session($response->id,$response->email,$response->role_type,$response->date,$response->secret_key);
				if($log_sessions == 1){
					redirect(site_url()."hotels/lists",'refresh');
				}
			}else{
				
				$this->session->set_flashdata('Error', $response->message);
				redirect(site_url(),'refresh');
				
			}
			
		}else{
			$this->session->set_flashdata('Error', 'Validation Error : '.strip_tags(trim(validation_errors())));
			redirect(site_url(),'refresh');
			
		}
	}

	/*Author: Nikhil Gowda
	  @Parms : null
	  Date : 27-03-2018
	  Instance : Authenticating Logout
	*/
	public function logout(){
        //$this->db->cache_delete_all();
       	$this->output->delete_cache('Hotels');
       	$this->output->delete_cache('Authenticate');
       	$this->output->delete_cache();
        $this->session->sess_destroy();
        redirect(base_url());
    }

}
