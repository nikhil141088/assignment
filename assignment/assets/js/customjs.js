function check_null_text_box(txt_id,box_name){
	if($("#"+txt_id).val()==""){
		$("#msg").html("<div class='alert alert-mini alert-danger margin-bottom-30'>"+box_name+" can't be empty.</div>");
		return false;
	}
	return true;
}
function validate(){
	var flag=true;
	flag=check_null_text_box("inputEmail","Email Id");
	if(!flag) return false;
	flag=check_null_text_box("inputPassword","Password");
	if(!flag) return false;
	return true;
}