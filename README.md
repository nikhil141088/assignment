## Assignment Application

The Application has Login system with two types of users Normal and Privilege Users. Users can login to the application and view list of hotels , search hotels by name,city,price where a privilege user have access to sort the hotels by name and price.

The application website is making use of API calls for Login , Get Hotels , Search Hotels , Sort Hotels.

---

## Application Setup

1. Xampp need to be installed to the local machine.
2. Install Xampp in C: Dirve.
3. In Xampp Control Panel start the Apache and Mysql Services.
3. In C: Drive -> xampp folder -> htdocs -> clone the assignment folder from bitbucket.
4. Go to browser , url - http://localhost/assignment/.
5. Finally the website application opens with the login screen.

---

## Technical Skills Used

1. PHP - CODEIGNITER FRAMEWORK.
2. JAVASCRIPT.
3. JQUERY.
4. CSS.
5. HTML.
6. REST API.

---

## Application Programing Interface (API) 

Used REST API with simple Authentication & Authorization (version 1.0). wsv1 acronym "Web Service Version 1".

**Product Key** : We have product key on every api request which identifies the product of our application and specifies the Test environment and Live environment. 

Currently we are using static product key which is of test environment.Use this on every API request.

**product_key** : **TEST-EF2356213F87785423FEJF32FHEF7WWDHWD**

product key will be generated in future as dynamic by user login to the console-developer-assignment site where this is the panel where user can edit is profile , Get Product key of TEST and LIVE , Track the API calls , Log the API's Calls.

---

## POSTMAN CHROME EXTENSION

**Collection Link : ** https://www.getpostman.com/collections/668094ca1d844fe7cbf6 

---

##1. Login API

**REQUEST URL : **
http://localhost/assignment/wsv1/login/ 

**HTTP REQUEST METHOD : **
POST

**POST FIELDS : **

productkey : TEST-EF2356213F87785423FEJF32FHEF7WWDHWD

method :  login

endpoint :  http://localhost/assignment/wsv1/login/

type :  POST 

email :  privilege1@gmail.com

password :  admin

**RESPONSE - JSON**

{
    "status": 200,
    "message": "Login Successfully",
    "id": 5,
    "email": "privilege3@gmail.com",
    "role_type": 2,
    "secret_key": "a5b1fe605f019c4573bf7ef28c3339d8",
    "date": "2018-03-28 18:38:34"
}

---

**LOGIN NORMAL USER DETAILS**

USERNAME : normal1@gmail.com | 
PASSWORD : admin

USERNAME : normal2@gmail.com |
PASSWORD : admin

USERNAME : normal3@gmail.com |
PASSWORD : admin



**LOGIN PRIVILEGE USER DETAILS**

USERNAME : privilege1@gmail.com |
PASSWORD : admin

USERNAME : privilege2@gmail.com |
PASSWORD : admin

USERNAME : privilege3@gmail.com |
PASSWORD : admin

---

## 2) Get Hotels List API

**REQUEST URL : **
http://localhost/assignment/wsv1/hotels_list/

**HTTP REQUEST METHOD : **
POST

**POST FIELDS**

user_id : 5 (Get the user id from the login response : id)

productkey : TEST-EF2356213F87785423FEJF32FHEF7WWDHWD

secretkey : a5b1fe605f019c4573bf7ef28c3339d8  (Get the secretkey from the login response : secret_key , this is used for authorization on every api.)

method : hotels_list

endpoint : http://localhost/assignment/wsv1/hotels_list/

type : POST

role_type : 2 (1-> Normal User , 2-> Privilege User) - Get it from the login response.


**RESPONSE - JSON**

{
    "status": 201,
    "message": "Hotel List",
    "list": "{\"hotels\":[{\"name\":\"Media One Hotel\",\"price\":102.2,\"city\":\"dubai\",\"availability\":[{\"from\":\"10-10-2020\",\"to\":\"15-10-2020\"},{\"from\":\"25-10-2020\",\"to\":\"15-11-2020\"},{\"from\":\"10-12-2020\",\"to\":\"15-12-2020\"}]},{\"name\":\"Rotana Hotel\",\"price\":80.6,\"city\":\"cairo\",\"availability\":[{\"from\":\"10-10-2020\",\"to\":\"12-10-2020\"},{\"from\":\"25-10-2020\",\"to\":\"10-11-2020\"},{\"from\":\"05-12-2020\",\"to\":\"18-12-2020\"}]},{\"name\":\"Le Meridien\",\"price\":89.6,\"city\":\"london\",\"availability\":[{\"from\":\"01-10-2020\",\"to\":\"12-10-2020\"},{\"from\":\"05-10-2020\",\"to\":\"10-11-2020\"},{\"from\":\"05-12-2020\",\"to\":\"28-12-2020\"}]},{\"name\":\"Golden Tulip\",\"price\":109.6,\"city\":\"paris\",\"availability\":[{\"from\":\"04-10-2020\",\"to\":\"17-10-2020\"},{\"from\":\"16-10-2020\",\"to\":\"11-11-2020\"},{\"from\":\"01-12-2020\",\"to\":\"09-12-2020\"}]},{\"name\":\"Novotel Hotel\",\"price\":111,\"city\":\"Vienna\",\"availability\":[{\"from\":\"20-10-2020\",\"to\":\"28-10-2020\"},{\"from\":\"04-11-2020\",\"to\":\"20-11-2020\"},{\"from\":\"08-12-2020\",\"to\":\"24-12-2020\"}]},{\"name\":\"Concorde Hotel\",\"price\":79.4,\"city\":\"Manila\",\"availability\":[{\"from\":\"10-10-2020\",\"to\":\"19-10-2020\"},{\"from\":\"22-10-2020\",\"to\":\"22-11-2020\"},{\"from\":\"03-12-2020\",\"to\":\"20-12-2020\"}]}]}"
}


---

## 3) Get Hotels Search API

**REQUEST URL : ** http://localhost/assignment/wsv1/hotel_search/

**HTTP REQUEST METHOD : **POST

**POST FIELDS**

user_id : 5 (Get the user id from the login response : id)

productkey : TEST-EF2356213F87785423FEJF32FHEF7WWDHWD

secretkey : a5b1fe605f019c4573bf7ef28c3339d8  (Get the secretkey from the login response : secret_key , this is used for authorization on every api.)

method : hotels_list

endpoint : http://localhost/assignment/wsv1/hotels_list/

type : POST

role_type : 2 (1-> Normal User , 2-> Privilege User) - Get it from the login response.

name : Novotel Hotel

city : cairo

price : 102


**RESPONSE - JSON**

{
"status":202,
"message":"Hotel Search List",
"list":{"hotels":[{"name":"Media One Hotel","price":102.2,"city":"dubai","availability":[{"from":"10-10-2020","to":"15-10-2020"},{"from":"25-10-2020","to":"15-11-2020"},{"from":"10-12-2020","to":"15-12-2020"}]},{"name":"Rotana Hotel","price":80.6,"city":"cairo","availability":[{"from":"10-10-2020","to":"12-10-2020"},{"from":"25-10-2020","to":"10-11-2020"},{"from":"05-12-2020","to":"18-12-2020"}]},{"name":"Novotel Hotel","price":111,"city":"Vienna","availability":[{"from":"20-10-2020","to":"28-10-2020"},{"from":"04-11-2020","to":"20-11-2020"},{"from":"08-12-2020","to":"24-12-2020"}]}]}
}


---


## 4) Get Hotels Sort API

**REQUEST URL : ** http://localhost/assignment/wsv1/hotel_sort/

**HTTP REQUEST METHOD : ** POST

**POST FIELDS**

user_id : 5 (Get the user id from the login response : id)

productkey : TEST-EF2356213F87785423FEJF32FHEF7WWDHWD

secretkey : a5b1fe605f019c4573bf7ef28c3339d8  (Get the secretkey from the login response : secret_key , this is used for authorization on every api.)

method : hotels_list

endpoint : http://localhost/assignment/wsv1/hotels_list/

type : POST

role_type : 2 (1-> Normal User , 2-> Privilege User) - Get it from the login response.

sort : name (name or price)


**RESPONSE - JSON**

{
"status":203,
"message":"Hotel Sort by name List",
"list":{"hotels":[{"name":"Concorde Hotel","price":79.4,"city":"Manila","availability":[{"from":"10-10-2020","to":"19-10-2020"},{"from":"22-10-2020","to":"22-11-2020"},{"from":"03-12-2020","to":"20-12-2020"}]},{"name":"Golden Tulip","price":109.6,"city":"paris","availability":[{"from":"04-10-2020","to":"17-10-2020"},{"from":"16-10-2020","to":"11-11-2020"},{"from":"01-12-2020","to":"09-12-2020"}]},{"name":"Le Meridien","price":89.6,"city":"london","availability":[{"from":"01-10-2020","to":"12-10-2020"},{"from":"05-10-2020","to":"10-11-2020"},{"from":"05-12-2020","to":"28-12-2020"}]},{"name":"Media One Hotel","price":102.2,"city":"dubai","availability":[{"from":"10-10-2020","to":"15-10-2020"},{"from":"25-10-2020","to":"15-11-2020"},{"from":"10-12-2020","to":"15-12-2020"}]},{"name":"Novotel Hotel","price":111,"city":"Vienna","availability":[{"from":"20-10-2020","to":"28-10-2020"},{"from":"04-11-2020","to":"20-11-2020"},{"from":"08-12-2020","to":"24-12-2020"}]},{"name":"Rotana Hotel","price":80.6,"city":"cairo","availability":[{"from":"10-10-2020","to":"12-10-2020"},{"from":"25-10-2020","to":"10-11-2020"},{"from":"05-12-2020","to":"18-12-2020"}]}]}
}

---

## 5) Get Status Code API

**REQUEST URL : ** http://localhost/assignment/wsv1/status_code?code=501

**HTTP REQUEST METHOD : ** GET

**RESPONSE - JSON**

{
    "status": 501,
    "message": "Invalid Authorization",
    "description": "Your Authorization id invalid"
}

---

## Status Codes

100 : User Email or Password is invalid.

200 : You have login successfully to the application.

201 : Get the list of hotels.

202 : Get the list of hotels based on search filters.

203 : Get the list of hotels based on sort filters.

207 : No Hotels found from the Api.

300 : Error on your request.

403 : Response Json is invalid.

410 : API method is invalid.

420 : Http Request Method is Invalid and the Api endpoint URL is invalid.

500 : Product Key is Invalid, Please contact admin.

501 : Your Authorization id invalid.

502 : User Role type is Invalid.

505 : Normal Users do not have permission for sort.

506 : Sort value should be name or price.

700 : Invalid GET request to the API.

900 : Validation Error of your request.
